# *`Vue`开发旅游网`App`*

#### 🍭*项目背景*

​	从`Vue`基础语法入手，逐层递进，更好的掌握`Vue`基础知识点。

#### :pencil:*简介*

- 相关插件
  - `Vue-router`路由配置
  - `Vuex`实现数据共享
  - `axios`获取接口数据
  - `Iconfont`字体库引入使用
  - `Vue-awesome-swiper`轮播组件
  - `fastclick`点击事件延时处理插件
  - `stylus`样式封装
  - `better-scroll`--拖拽滚动插件
  - `Velocity.js`--`js`动画库
- 环境
  - `vue 2.6.6`
  - `vue/cli: "^3.5.0`
  - `node.js 10.14`

#### 🌏*项目部署*

1 安装依赖

```shell
npm instal
```

2 编译项目和热加载

```shell
npm run serve
```

3 编译和打包

```
npm run build
```

#### 👀*项目截图：*

- 首页

   ![首页](http://ww1.sinaimg.cn/mw690/98bbea39ly1g54glblvuyj20ms0yawvx.jpg)
    
    ![](http://ww1.sinaimg.cn/mw690/98bbea39ly1g54gyiyb1nj20mc0xk7wh.jpg)

-  选择城市页

   点击城市首页切换到相应城市，点击字母跳转字符对应城市，滑动右侧字母条左侧城市列表联动，支持搜索城市，显示城市列表。
![](http://ww1.sinaimg.cn/mw690/98bbea39ly1g54gqvzu39j20mu0yedhf.jpg)
  
- 热门推荐详情页
![](http://ww1.sinaimg.cn/mw690/98bbea39ly1g54gt48pnrj20mg0xm4ff.jpg)
   点击图片弹出画廊页

   
#### :smile:*说明*

> ***本项目参考：`MOOC`网 教程《`vue2.5`开发去哪儿网`App`  》***