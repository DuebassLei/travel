import Vue from 'vue'
import Vuex from 'vuex'
import  state from './state'
import mutations from "./mutations";
Vue.use(Vuex)

export default new Vuex.Store({
  // state: state,
  // mutations: mutations,
  //上面 {key ,value}一样，省略后面部分，简写如下
    state,
    mutations,
    actions: {
    // changeCity(ctx,city){
    //   //ctx 传递的上下文
    //   ctx.commit('changeCity',city)
    // }
    }
})
