import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import City from './views/City'
import Detail from './views/Detail'

Vue.use(Router)

export default new Router({
    scrollBehavior:()=>({ x: 0, y: 0 }),
    routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
      {
          path:'/city',
          name:'city',
          component:City
      },{
          path:'/detail/:id',
          name:'detail',
          component: Detail
      }
    // {
    //   path: '/about',
    //   name: 'about',
    //   component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    // }
  ]
})

